#!/usr/bin/env python3

"""
make pheweb's pheno-list.json from csv

The CSV file is downloaded from steve's google sheets
"""

import csv
import os
import sys
import argparse
import json

parser = argparse.ArgumentParser(description='make a pheno-list.json file from your data and a csv file')
parser.add_argument('-e', type=str, help='the extension of the files ready for pheweb processing')
parser.add_argument('-d', type=str, help='the data directory with pheno files')
parser.add_argument('-c', type=str, help='the csv file with the pheno names and descriptions')
args = parser.parse_args()

def main():
	"""
	run the main program
	"""
	phenos = []
	phenoFilePath = os.path.join(os.path.abspath(args.d), "pheno-list.json")
	with open(os.path.abspath(args.c), newline='') as csvf:
		csvreader = csv.reader(csvf)
		csvdata = list(csvreader)

	for file in os.listdir(os.path.abspath(args.d)):
		if file.endswith(args.e):
			fullPath = os.path.join(os.path.abspath(args.d), file)
			baseName = file.split(args.e)[0]
				
			for row in csvdata:
				if row[0] == baseName:
					pcode = row[2].replace("+", "_").replace("(", "",).replace(")", "").replace(" ", "_")
					pstring = pcode.replace("_", " ")
					phenos.append({
						"assoc_files": ["./{}{}".format(baseName, args.e)],
						"phenocode": pcode,
						"phenostring": pstring,
						"category": row[6] 	
						})
					break
			
			
	with open(phenoFilePath, 'w') as outf:
		json.dump(phenos, outf)

if __name__ == "__main__":
	sys.exit(main())
