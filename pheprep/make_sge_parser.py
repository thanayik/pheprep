import json
import os
import sys
import argparse
import pheweb


parser = argparse.ArgumentParser(description='make an sge job list for use with fsl_sub')
parser.add_argument('-d', type=str, help='the data dir with all your pheno files')
parser.add_argument('-j', type=str, help='the pheno-list.json file')
args = parser.parse_args()

def main():
	phewebBin = os.path.dirname(
		os.path.dirname(
			os.path.dirname(
				os.path.dirname(
					os.path.dirname(pheweb.__file__)))))
	phewebBin = os.path.join(phewebBin, "bin", "pheweb")
	print(phewebBin)

	with open(os.path.abspath(args.j)) as json_file:
		nphenos = len(json.load(json_file))
	print(nphenos)
	with open(os.path.join(os.path.abspath(args.d), "sge_parse_phenos.sh"), 'a') as sgefile:
		sgefile.write(
'''\
#!/bin/bash
#
#$ -N parsePhenos
#$ -t 1-{}
#$ -o {}
#$ -e {}
#$ -P win.prjc
#$ -l h_vmem=16G

# bash is zero indexed, but task_id cannot be zero
INDEX=$((SGE_TASK_ID-1))

source ~/.bashrc
'''.format(
	nphenos,
	os.path.join(os.path.abspath(args.d), 'sgelogs', 'parse.sgeout'),
	os.path.join(os.path.abspath(args.d), 'sgelogs', 'parse.sgeerr')
	)
)

		sgefile.write(
			'jobs=('
		)
		sgefile.write(' '.join(map(str, list(range(nphenos)))))
		sgefile.write(')\n\n')
		sgefile.write("export PHEWEB_DATADIR={}; cd $PHEWEB_DATADIR; {} conf num_procs=1 parse --phenos=".format(
			os.path.abspath(args.d),
			phewebBin))
		sgefile.write("${jobs[$INDEX]}\n")


if __name__ == "__main__":
	sys.exit(main())
