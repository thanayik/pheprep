#!/bin/bash
#
#$ -N phewebFinal
#$ -o /gpfs2/well/win/users/jwz430/phewebFull/data/sgelogs/phewebFin.sgeout
#$ -e /gpfs2/well/win/users/jwz430/phewebFull/data/sgelogs/phewebFin.sgeerr
#$ -P win.prjc
#$ -pe shmem 40

source ~/.bashrc

export PHEWEB_DATADIR=/gpfs2/well/win/users/jwz430/phewebFull/data; cd $PHEWEB_DATADIR; /gpfs2/well/win/users/jwz430/pyenvs/pheweb/bin/pheweb conf num_procs=40 process --no-parse
