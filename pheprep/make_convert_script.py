#!/usr/bin/env python3

"""
make convert script
"""

import csv
import os
import sys
import argparse
import json
import shutil
import tempfile

parser = argparse.ArgumentParser(description='make an sge task job list for converting many files for use with pheweb')
parser.add_argument('-e', type=str, help='the extension of the unconverted files')
parser.add_argument('-d', type=str, help='the data directory with files ready to be used in `pheweb process`')
parser.add_argument('-p', type=str, help='the path to the python conversion script')
parser.add_argument('-v', type=str, help='the path to the variants file')
args = parser.parse_args()

def main():
	"""
	run the main program
	"""
	allInFiles = []
	allTempVarFiles = []
	allOutFiles = []
	for file in os.listdir(os.path.abspath(args.d)):
		if file.endswith(args.e):
			fullPath = os.path.join(os.path.abspath(args.d), file)
			baseName = file.split(args.e)[0]
			outFile = os.path.join(os.path.abspath(args.d), baseName + ".tsv")
			
			#vdir = os.path.dirname(os.path.abspath(args.v))
			#tempfile.tempdir = vdir
			#tvfile = tempfile.mktemp()
			#shutil.copy(os.path.abspath(args.v), tvfile)

			allInFiles.append(fullPath)
			#allTempVarFiles.append(tvfile)
			allOutFiles.append(outFile)
	
	with open(os.path.join(os.path.abspath(args.d), "sge_convert.sh"), 'a') as sgefile:
		#module load Python/3.7.4-GCCcore-8.3.0;
		sgefile.write(
'''\
#!/bin/bash
#
#$ -N phewebConvert
#$ -t 1-{}
#$ -tc 100
#$ -o {}
#$ -e {}
#$ -l h_vmem=10G

# bash is zero indexed, but task_id cannot be zero
INDEX=$((SGE_TASK_ID-1))

source ~/.bashrc
'''.format(
	len(allInFiles),
	os.path.join(os.path.abspath(args.d), 'sgelogs', 'sgeout'),
	os.path.join(os.path.abspath(args.d), 'sgelogs', 'sgeerr')
	)
)

		sgefile.write(
			'infiles=('
		)
		sgefile.write(' '.join(allInFiles))
		sgefile.write(')\n\n')
		sgefile.write('outfiles=(')
		sgefile.write(' '.join(allOutFiles))
		sgefile.write(')\n\n')
		sgefile.write('vfile={}\n\n'.format(os.path.abspath(args.v)))
		#sgefile.write(' '.join(allTempVarFiles))
		#sgefile.write(')\n\n')
		#sgefile.write(
		#	"echo ${infiles[$INDEX]} ${tvfiles[$INDEX]} ${outfiles[$INDEX]}\n"
		#	)

		sgefile.write(
			'python3 {} '.format(os.path.abspath(args.p)))
		sgefile.write(
			"-i ${infiles[$INDEX]} -v $vfile > ${outfiles[$INDEX]}\n"
			)
		sgefile.write(
			"gzip ${outfiles[$INDEX]}\n"
			)

			
if __name__ == "__main__":
	sys.exit(main())
