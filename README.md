# Pheprep

Pheprep is a collection of scripts that help convert UK Bio Bank data from Steve and Lloyd into a format accepted by [pheweb](https://github.com/statgen/pheweb). Pheprep scripts also help users on SGE clusters submit some pheweb stages as a collection of parallel jobs.

# Workflow

1) Get data from Steve and Lloyd
2) Parse and write new data files
3) Make pheno-list.json
4) Run [Taylor's SGE version]() of `pheweb parse`
5) Run `pheweb process --no-parse`
6) Copy pheweb data to web server
7) Start pheweb [web server container](https://git.fmrib.ox.ac.uk/web_configs/ukbb_container)

# Get Data from Steve and LLoyd

The phenotype text data from Steve and LLoyd are in "Steve and LLoyd format". Here's an example of what you would see in these phenotype files:

HEAD:
```
head 0001.txt
chr rsid pos a1 a2 beta se pval(-log10)
01 rs367896724 10177 A AC -0.001157 0.014236 0.029085
01 rs201106462 10352 T TA -0.0020071 0.014645 0.050126
01 rs534229142 10511 G A -0.08748 0.19528 0.18431
01 1:10616_CCGCCGTTGCAAAGGCGCGCCG_C 10616 CCGCCGTTGCAAAGGCGCGCCG C 0.073579 0.091994 0.37282
01 rs575272151 11008 C G -0.033773 0.024695 0.76589
01 rs544419019 11012 C G -0.033773 0.024695 0.76589
01 rs540538026 13110 G A 0.058805 0.033066 1.1229
01 rs62635286 13116 T G 0.014839 0.019169 0.35765
01 rs62028691 13118 A G 0.014839 0.019169 0.35765
```

TAIL:
```
tail 0001.txt
0X rs3093527 155236918 G A 0.038974 0.067683 0.24815
0X rs764129350 155237811 G A 0.38364 0.19183 1.3418
0X rs776508517 155237885 A G -0.11681 0.13271 0.42166
0X rs751554334 155247345 T C -0.041586 0.089541 0.19223
0X rs4568757 155254802 C A 0.057125 0.078198 0.33247
0X rs28415761 155254881 C A 0.20245 0.12593 0.96684
0X rs28742024 155255153 G A -0.0044436 0.11968 0.013057
0X rs1045930 155255277 C G 0.038864 0.16063 0.092148
0X X:155260480_T_A 155260480 T A 0.026299 0.015218 1.0759
0X X:155260480_T_G 155260480 T G 0.0071289 0.016204 0.18047
```

There are a few problems in this format that will cause pheweb to fail. 

1) The column delimiter is a space and not a tab.
2) pheweb expects unscaled pvalues. These are -log10 scaled (need to undo).
3) The columns names need to be renamed to what pheweb expects. The column order does not matter.
4) Pheweb does its own rsid lookup. We need to drop the rsid column.
5) the first column (chr) contains numbers and strings left padded with a single zero where needed. Pheweb does not like this. We must remove all left pad zeros.
6) There is no maf and af info because it is redundant across files. However, we need to put that info back into each phenotype file here so that pheweb can use it correctly. This comes from a `variants.txt` file.

# Parse and write new data files

We need to parse the Steve and Lloyd format and write out new files to address the problems pointed out earlier.

Here's what the parser script (`pheprep.py`) does for one input file `0001.txt.gz`:

Run program: `python3 pheprep.py -i 0001.txt.gz -v variants.txt.gz`

1) check that both files have equal number of lines
2) open both files for reading
3) read a line from each file (same line index)
4) parse that line into a list of fields
5) -i file: remove padding zero from `chr` field
6) -i file: scale `pval` to original scale (remove -log10)
7) -v file: make `maf` variable using `min(af, 1-af)`
8) write new header: chr-->chrom, pos-->pos, a1-->ref, a2-->alt, beta-->beta, se-->sebeta, pval(-log10)-->pval, maf, af
9) write each new line with necessary values **tab delimited**

## SGE parse and write

The `pheprep.py` script can be run independently for each phenotype file.

I have created a separate python script `make_convert_script.py` that generates a bash script `sge_convert.sh`. This bash script sets up the SGE array job that can be submitted to a queue.

Example queue submission: `qsub -q short.qe sge_convert.sh`

`make_convert_script.py` usage:

```
Example:

python make_convert_script.py -e .txt.gz -d ./data -p ../pheprep.py -v ./data/variants.txt.gz

usage: make_convert_script.py [-h] [-e E] [-d D] [-p P] [-v V]

make an sge task job list for converting many files for use with pheweb

optional arguments:
  -h, --help  show this help message and exit
  -e E        the extension of the unconverted files
  -d D        the data directory with files ready to be used in `pheweb
              process`
  -p P        the path to the python conversion script
  -v V        the path to the variants file
```

Here's what a the new files look like when they have been properly prepared for input to `pheweb`:

HEAD:
```
chrom	pos	ref	alt	beta	sebeta	pval	maf	af
1	10177	A	AC	-0.001157	0.014236	0.9352226147136065	0.400522	0.400522
1	10352	T	TA	-0.0020071	0.014645	0.8909924008180645	0.39659	0.39659
1	10511	G	A	-0.08748	0.19528	0.6541690605965481	0.0013354	0.0013354
1	10616	CCGCCGTTGCAAAGGCGCGCCG	C	0.073579	0.091994	0.4238185877576885	0.005900000000000016	0.9941
1	11008	C	G	-0.033773	0.024695	0.17143914810708835	0.084682	0.084682
1	11012	C	G	-0.033773	0.024695	0.17143914810708835	0.084682	0.084682
1	13110	G	A	0.058805	0.033066	0.07535290502354003	0.057932	0.057932
1	13116	T	G	0.014839	0.019169	0.4388842542088913	0.1889	0.1889
1	13118	A	G	0.014839	0.019169	0.4388842542088913	0.1889	0.1889
```

# Make pheno-list.json

We need to generate the `pheweb` `pheno-list.json` file. This should be done after converting from Steve and Lloyd format to an acceptable pheweb format (see the above step).

`pheno-list.json` controls what data pheweb works with, and it controls how phenotype strings will appear on the web site using the `phenostring` key. The `phenostring` **must** contain spaces between keys or words for search to work well on the web site. **Do not use one long string with underscores separating keys or words**. 

Here's one entry from an example `pheno-list.json` file:

```
[
    ... more entries above ...

    {
        "assoc_files": ["./2894.tsv.gz"],
        "phenocode": "rfMRI_connectivity_ICA100_edge_466",
        "phenostring": "rfMRI connectivity ICA100 edge 466",
        "category": "rfMRI connectivity"
    },

    ... more entries below ...
]
```
I generate the `pheno-list.json` file by using the CSV downloaded from [here](https://docs.google.com/spreadsheets/d/1zLoGcat_CsNAv2t6g_qNbyY6r0_62ce345be5yej_wU/edit#gid=2074925838), and the available files in the data directory. The data directory will contain your files that have been created by running `pheprep`. 

Make the `pheno-list.json` file by using my `make_phenolist.py` script.

```
Example:

python make_phenolist.py -e .tsv.gz -d ./data -c BIG40-IDPs-res.csv

usage: make_phenolist.py [-h] [-e E] [-d D] [-c C]

make a pheno-list.json file from your data and a csv file

optional arguments:
  -h, --help  show this help message and exit
  -e E        the extension of the files ready for pheweb processing
  -d D        the data directory with pheno files
  -c C        the csv file with the pheno names and descriptions
```

# Run SGE version of pheweb parse

Now we finally get to the stage where we can start using pheweb to do some processing. 

Pheweb parse will check the data and make a renamed copy of it in the `generated_by_pheweb/parsed` directory. 

I have written `make_sge_parser.py` which generates a bash script `sge_parse_phenos.sh` that can be submitted to an SGE cluster. 

Example queue submission: `qsub -q short.qe sge_parse_phenos.sh`

`make_sge_parser.py` usage:

```
Example:

python make_sge_parser.py -d ./data -j ./data/pheno-list.json

usage: make_sge_parser.py [-h] [-d D] [-j J]

make an sge job list to submit to the cluster

optional arguments:
  -h, --help  show this help message and exit
  -d D        the data dir with all your pheno files
  -j J        the pheno-list.json file
```

# Run pheweb process --no-parse

Now that we have run `pheweb parse` (the SGE version) on the cluster we can run pheweb's remaining stages.

I have created a simple script (`sge_pheweb_process.sh`) that can be submitted to the SGE queue.

If there are RAM limits issues then request more slots, or keep the slots and reduce the number of concurrent jobs that pheweb does (in `config.py`).

```
#!/bin/bash
#
#$ -N phewebFinal
#$ -o /gpfs2/well/win/users/jwz430/phewebFull/data/sgelogs/phewebFin.sgeout
#$ -e /gpfs2/well/win/users/jwz430/phewebFull/data/sgelogs/phewebFin.sgeerr
#$ -P win.prjc
#$ -pe shmem 40

source ~/.bashrc

export PHEWEB_DATADIR=/gpfs2/well/win/users/jwz430/phewebFull/data; cd $PHEWEB_DATADIR; /gpfs2/well/win/users/jwz430/pyenvs/pheweb/bin/pheweb conf num_procs=40 process --no-parse

```


